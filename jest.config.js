module.exports = {
  testPathIgnorePatterns:['/node_modules/','__tests__/.*'],
  setupFiles:[
      'raf/polyfill', //requestAnimationFrame polyfill buat node
      '<rootDir>/src/__tests__/setup-enzyme.js'
  ],
  snapshotSerializers:[
      'enzyme-to-json/serializer'
  ]
};