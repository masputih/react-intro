import React from 'react';

const Header = (props) => (
  <div className="app-header">
    <h1>{props.title}</h1>
    <p>{props.date}</p>
  </div>  
);

export default Header;