import React from 'react';

const Footer = ( props ) => (
  <div>
    <p>{props.text}</p>
  </div>
);

export default Footer;