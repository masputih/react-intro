import React from 'react';
import Header from './Header.jsx';
import Footer from './Footer.jsx';
import List from './List.jsx';
import Status from './Status.jsx';

export default class Notepad extends React.Component{

  constructor(props){
    super(props);
    this.date = new Date();
    this.listdata = ['one','two','three'];
    
    this.state = {
      isLoggedIn:false
    };

    this.onLogin = this.onLogin.bind(this);
    this.onLogout = this.onLogout.bind(this);

  }

  onLogin(e){
    
    this.setState({
      isLoggedIn: true
    });
    e.preventDefault();
  }

  onLogout(e){
    
    this.setState({
      isLoggedIn: false
    });
    e.preventDefault();
  }

  componentWillMount(){
    console.log('will mount');
  }

  componentDidMount(){
    console.log('did mount');
  }

  componentWillUpdate(nextProps, nextState){
    console.log('will update', nextProps, nextState);
  }

  componentDidUpdate(prevProps, prevState){
    console.log('did update', prevProps, prevState);
  }

  render(){
    return (
      <div>         
        <Header title={'My notepad'} 
          date={this.date.toLocaleDateString() } />

        <Status 
          loggedIn={this.state.isLoggedIn}
          onLogin={this.onLogin}
          onLogout={this.onLogout} />


        

        <Footer text={'FOOTER'} />
      </div>
    );
  }
};