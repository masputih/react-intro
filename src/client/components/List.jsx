import React from 'react';

const List = ( props ) => {

  const items = props.data.map((el,idx)=>{
    return <li key={idx}>{el}</li>;
  });

  if(items.length > 0){
    return (
      <div> 
        <ul>{items}</ul>
       </div>
    ); 
  }

  return (
    <p>Empty List</p>
  );
  
};

export default List;