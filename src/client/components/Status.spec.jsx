import React from 'react';
import { shallow } from 'enzyme';
import Status from './Status.jsx';


test('Status loggedIn', ()=>{
  const wrapper = shallow(<Status loggedIn={true} />);
  expect(wrapper).toMatchSnapshot();
});


test('Status loggedOut', ()=>{
  const wrapper = shallow(<Status loggedIn={false} />);
  expect(wrapper).toMatchSnapshot();
});
