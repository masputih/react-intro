import React from 'react';

const Status = ( props ) => {

  if(props.loggedIn){
    return (
      <div className="status"> 
        <p>Status: User</p>
        <button onClick={props.onLogout}>Logout</button>
      </div>
      
    );
  }

  return (
    <div className="status"> 
      <p>Status: Guest</p>
      <button onClick={props.onLogin}>Login</button>
    </div>
  );
};

export default Status;