import React from 'react';

const URLInput = (props) => {

  let url = props.url;

  return (
    <div>
      <input type="text" 
      onChange={(e) => {
        url = e.target.value;
      }} 
      value={props.url}
      />
      <button onClick={() => {
        props.onSubmit(url);
      }}> Submit</button>
    </div>
  );
};

export default URLInput;