import React from 'react';

export default class VideoWrapper extends React.Component{

  constructor(props){
    super(props);
    this.videoEl;
  }

  componentDidMount(){
    this.videoEl.addEventListener('playing', this.props.onVideoPlaying );
    this.videoEl.addEventListener('pause', this.props.onVideoPaused );
    this.videoEl.addEventListener('durationchange',this.props.onDurationChanged);
    this.videoEl.addEventListener('timeupdate', this.props.onTimeUpdate);
  }

  shouldComponentUpdate(nextProps, nextState){
    if(nextProps.action === 'play'){
      this.videoEl.play();      
    }else if(nextProps.action === 'pause'){
      this.videoEl.pause();
    }
    return true;
  }

  render(){
    return (
      <div> 
        <video src={this.props.url} 
          ref={(video )=> {this.videoEl = video; } } />   
      </div>
    );
  }
};