import React from 'react';

const Timelabel = ( props ) => (
  <div>
    <p>{props.time} / {props.duration} </p>  
   </div>
);

export default Timelabel;