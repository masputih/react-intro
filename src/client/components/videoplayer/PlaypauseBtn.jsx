import React from 'react';

const PlaypauseBtn = ( props ) => {
  if(props.status === 'playing'){
    return (
      <button onClick={props.onPause}>Pause</button>
    );
  }

  return <button onClick={props.onPlay}>Play</button>;
};

export default PlaypauseBtn;