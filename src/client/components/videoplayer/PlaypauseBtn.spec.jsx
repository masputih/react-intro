import React from 'react';
import {shallow} from 'enzyme';
import PlaypauseBtn from './PlaypauseBtn';

test('PlaypauseBtn, status: playing ', ()=>{
  const wrapper = shallow(<PlaypauseBtn status={'playing'}/>);
  expect(wrapper).toMatchSnapshot();
});

test('PlaypauseBtn, status: paused ', ()=>{
  const wrapper = shallow(<PlaypauseBtn status={'paused'}/>);
  expect(wrapper).toMatchSnapshot();
});