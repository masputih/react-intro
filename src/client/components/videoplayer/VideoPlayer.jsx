import React from 'react';
import VideoWrapper from './VideoWrapper.jsx';
import PlaypauseBtn from './PlaypauseBtn.jsx';
import Timelabel from './Timelabel.jsx';
import URLInput from './URLInput.jsx';

export default class VideoPlayer extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      url:'',
      status:null,
      duration:0,
      time:0,
      action:null
    };

    this.onURLSubmit = this.onURLSubmit.bind(this);
    this.onPlay = this.onPlay.bind(this);
    this.onPause = this.onPause.bind(this);
    this.onVideoPaused = this.onVideoPaused.bind(this);
    this.onVideoPlaying = this.onVideoPlaying.bind(this);
    this.onDurationChanged = this.onDurationChanged.bind(this);
    this.onTimeUpdate = this.onTimeUpdate.bind(this);
  }

  onURLSubmit(data){
    this.setState({
      url: data
    });
  }

  onPlay(e){
    this.setState({
      action:'play'
    });
    e.preventDefault();
  }

  onPause(e){
    this.setState({
      action:'pause'
    });
    e.preventDefault();
  }

  onVideoPlaying(e){
    console.log('Video playing');
    this.setState({
      status:'playing'
    });
  }

  onVideoPaused(e){ 
    console.log('Video paused');
    this.setState({
      status:'paused'
    });
  }

  onDurationChanged(e){
    console.log('duration changed', e.target.duration);
    this.setState({
      duration: e.target.duration
    });
  }

  onTimeUpdate(e){
    console.log('time update', e.target.currentTime);
    this.setState({
      time: e.target.currentTime
    });
  }

  render(){
    return (
      <div> 
        <URLInput onSubmit={this.onURLSubmit} 
          url={'https://vjs.zencdn.net/v/oceans.mp4'}/>
        
        <VideoWrapper url={this.state.url} 
          action={this.state.action} 
          onVideoPlaying={this.onVideoPlaying}
          onVideoPaused={this.onVideoPaused}
          onDurationChanged={this.onDurationChanged}
          onTimeUpdate={this.onTimeUpdate}
          />  

        <PlaypauseBtn 
          status={this.state.status}
          onPlay={this.onPlay}
          onPause={this.onPause}
        />

        <Timelabel time={this.state.time} 
          duration={this.state.duration}/>
       </div>
    );
  }
};