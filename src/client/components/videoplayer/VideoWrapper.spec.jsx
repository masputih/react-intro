import React from 'react';
import { mount } from 'enzyme';
import VideoWrapper from './VideoWrapper.jsx';

test('VideoWrapper', ()=>{
  const wrapper = mount(<VideoWrapper />);
  expect(wrapper).toMatchSnapshot();
});