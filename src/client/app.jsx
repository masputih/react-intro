import React from 'react';
import ReactDOM from 'react-dom';
import VideoPlayer from './components/videoplayer/VideoPlayer.jsx';

import './styles/styles.scss';

ReactDOM.render( <VideoPlayer />, document.getElementById('app') );
